<?php

get_template_part( 'admin/autoload' );

// Enqueue scripts
function theme_scripts() {
	$ver = '1.0';
	wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/css/main.css', null, $ver );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );


// Register primary navigation
if ( ! function_exists( 'theme_setup' ) ) {
	function theme_setup() {

		//navigations
		register_nav_menus( array(
			'primary' => 'Primary Menu',
		));

		//sidebars
		register_sidebar(array(
			'name' 	=> 'Page Sidebar',
			'id'	=> 'page-sidebar',
			'description'	=> 'Widgets for the sidebar on each page.',
			'before_widget'	=> '<div class="aside widget %2$s" id="%1$s">',
			'after_widget'	=> '</div>',
			'before_title'  => '<h4 class="widgettitle">',
			'after_title'   => '</h4>'
		));
	}
}
add_action( 'after_setup_theme', 'theme_setup' );

function new_excerpt_more($more) {
	global $post;
	return '... <a class="more-link" href="' . get_permalink($post->ID) . '">read more &raquo;</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

//edit user capibilities
function add_options_to_editor () {
	$role = get_role( 'editor' );
	$role->add_cap( 'edit_theme_options' ); 
	$role->add_cap( 'gravityforms_edit_forms' );
	$role->add_cap( 'gravityforms_delete_forms' );
	$role->add_cap( 'gravityforms_create_form' );
	$role->add_cap( 'gravityforms_view_entries' );
	$role->add_cap( 'gravityforms_edit_entries' );
	$role->add_cap( 'gravityforms_delete_entries' );
	$role->add_cap( 'gravityforms_view_settings' );
	$role->add_cap( 'gravityforms_edit_settings' );
	$role->add_cap( 'gravityforms_export_entries' );
	$role->add_cap( 'gravityforms_view_entry_notes' );
	$role->add_cap( 'gravityforms_edit_entry_notes' );
}
add_action( 'admin_init', 'add_options_to_editor' );

// favicon
function add_favicon() {
  	$favicon_url = get_stylesheet_directory_uri() . '/imgs/favicon.ico';
	echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}   
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');
add_action('wp_head', 'add_favicon');

// mobile detection
function mobile_viewport() {
	echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
}
add_action('wp_head', 'mobile_viewport');

// for debuging
function debug($data) {
	echo '<pre>';
	var_dump($data);
	echo '</pre>';
}

// returns the top parent page id
function get_top_parent_page_id() {
	global $post;
	if (isset($post)) {
		if ($post->ancestors) {
			$post_ancestors = get_post_ancestors($post);
			return end( $post_ancestors );
		} else {
			return $post->ID;
		}
	}
}

if( function_exists('acf_add_options_page') ) {	
	acf_add_options_page(array(
		'page_title' 	=> 'Global Settings',
		'menu_title'	=> 'Global Settings',
		'menu_slug' 	=> 'global-settings',
		'capability'	=> 'edit_pages',
		'redirect'		=> false
	));
}

function field_exists($field) {
	if (isset($field) && strlen($field) > 0) {
		return true;
	} else {
		return false;
	}
}

// get video data given a vimeo or youtube url
function get_video_data($video_url) {
	// parse url
	$parse = parse_url($video_url);

	// extract video id
	if ($parse['query']) {	
		parse_str($parse['query'], $query);
		$data['id'] = $query['v'];
	} else {
		$video_array = explode('/', $video_url);
		$data['id'] = array_pop($video_array);
	}
	// extract video type
	$data['type'] = str_replace(array('.com', 'www.'), '', $parse['host']);

	return $data;
}

// echos a clean excerpt based on how many words
function get_clean_excerpt($post_id, $words, $link = false, $append = false) {
    $post = get_post($post_id);
	if ($link === true) {
		$append = ' <a href="' . get_permalink($post_id) . '">' . $append . '</a>';
	}
	$content = $post->post_content;
    $content = strip_shortcodes($content);
    return wp_trim_words(esc_html(strip_tags($content)), $words, $append);
}

// echo out a navigation in the content with shortcode
// ex: [nav name="primary"]
function nav_func( $atts = array(), $content = '' ) {
	$atts = shortcode_atts( array(
		'name' => 'primary',
	), $atts, 'nav' );

	return wp_nav_menu( array( 'theme_location' => $atts['name'], 'echo' => false ) );
}
add_shortcode( 'nav', 'nav_func' );