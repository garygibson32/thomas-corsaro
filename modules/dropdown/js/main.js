
jQuery(document).ready(function() {	

	jQuery('.dropdown ul.sub-menu li.menu-item-has-children > a').append(' &raquo;');
	jQuery('.dropdown div > ul > li').hoverIntent(
	    function () {
	        jQuery(this).addClass('hover-menu');
	        jQuery('> ul', this).stop(false,true).slideDown(100);
	    }, 
	    function () {
	        jQuery(this).removeClass('hover-menu');
	        jQuery('> ul', this).stop(false,true).slideUp(100);
	    }
	);

	// sub drop-down menu
	jQuery('.dropdown div > ul > li ul > li').hoverIntent(
	    function () {
	        jQuery(this).addClass('hover-menu');
	        jQuery('> ul', this).stop(false,true).animate({width:'toggle'},100);
	    }, 
	    function () {
	        jQuery(this).removeClass('hover-menu');
	        jQuery('> ul', this).stop(false,true).animate({width:'toggle'},100);
	    }
	);
});