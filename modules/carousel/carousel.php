<?php 

// Enqueue files
function wdoy_carousel_files() {	
	wp_enqueue_script( 'carousel', get_template_directory_uri() . '/modules/carousel/js/slick.min.js', null, null, true );
}
add_action( 'wp_enqueue_scripts', 'wdoy_carousel_files' );
