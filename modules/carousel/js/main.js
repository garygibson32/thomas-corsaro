
jQuery(document).ready(function() {
	jQuery('.carousel').slick({
		arrows: false,
		speed: 500,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
	});
});