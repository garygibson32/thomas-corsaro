
jQuery(document).ready(function() {
	jQuery('.arrow-up__wrapper').click(function(e) {
	    e.preventDefault();
	    jQuery("body, html").animate({ scrollTop:0 }, 
	    {
	        duration: 1200
	    });
	});
	var offset = 220;
	var duration = 500;
	jQuery(window).scroll(function() {
	    if (jQuery(this).scrollTop() > offset) {
	        jQuery('.arrow-up__wrapper:not(.arrow-up--state-visible)').stop().animate({
	            opacity : 0.7,
	        }, duration).addClass('arrow-up--state-visible');
	    } else {
	        jQuery('.arrow-up__wrapper.arrow-up--state-visible').stop().animate({
	            opacity : 0,
	        }, duration).removeClass('arrow-up--state-visible');
	    }
	});
});