<?php 

// Enqueue files
function wdoy_slider_files() {	
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/modules/slider/js/jquery.flexslider-min.js', 'jquery', null, true );
}
add_action( 'wp_enqueue_scripts', 'wdoy_slider_files' );


// Set image size
add_image_size( 'flexslider', 1200, 500, true );


// [wdoy-slider]
function wdoy_slider_shortcode() {
	if ($slides = get_field( 'slideshow' )) {
		$output = '<div class="flexslider pattern clear grid">';
			$output .= '<ul class="slider">';
				if ($slides) :
		            foreach ($slides as $slide) :
						$output .= '<li class="slide">';
							$data = array(
								'image_id' => $i = $slide['image'], 
								'image_src' => wp_get_attachment_image_src( $i, 'full' ),
								'title' => $slide['title'],
								'description' => $slide['description'],
								'url' => $slide['url']
							);

							// debug($data);
                            $output .= '<div class="slide__image-wrapper" style="background-image: url(' . $data['image_src'][0] . ')">';
								$output .= wp_get_attachment_image( $data['image_id'], 'flexslider', null, array('class' => 'slide__image' ) );
                    		$output .= '</div>';  
            				if (isset($data['title'])) : 
                                $output .= '<div class="slide__content-wrapper">';
                                    $output .= '<div class="slide__content">';
                						$output .= '<h3>' . $data['title'] . '</h3>';
                						$output .= '<p>' . $data['description'] . '</p>';
                						if ($data['url']) {
                							$output .= '<div class="button">';
	                							$output .= '<a class="button" href="' . $data['url'] . '">';
	                								$output .= 'More on this';
	            								$output .= '</a>'; 
	            							$output .= '</div>';
            							}
                                    $output .= '</div>';
                                $output .= '</div>';
            				endif;              
		                $output .= '</li>';
		            endforeach;	
		        endif;
			$output .= '</ul>';
		$output .= '</div>';

		return $output;
	}
}
add_shortcode( 'wdoy-slider', 'wdoy_slider_shortcode' );


// Advanced Custom Fields (USE EXPORT FILE INSTEAD)
// include( plugin_dir_path( __FILE__ ) . 'acf-fields.php' );
