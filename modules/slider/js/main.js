jQuery(document).ready(function() {
	jQuery('.flexslider').flexslider({
		namespace: "flex-",
		selector: ".slider > li",
		animation: "fade",
		easing: "swing",
		slideshowSpeed:7000,
		animationSpeed:900,
		controlNav:true,
		directionNav:false
	});
});