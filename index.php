<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php bloginfo('name'); ?> <?php wp_title( '|', true, 'left' ); ?></title>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="header">
        <div class="header__top">
            <div class="grid">
                <p>200 Pine Grove Commons, York, PA 17403</p>
                <nav class="social">
                    <ul>
                        <li>facebook</li>
                        <li>google+</li>
                        <li>yelp</li>
                        <li>foursquare</li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="header__middle">
            <div class="grid">
                <h1 class="header__middle_logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                </h1>
                <nav class="header__middle_nav">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                </nav>
            </div>
        </div>
        <div class="header__bottom">
          <div class="grid">
            <p><i>phone</i>(717) 741-5700</p>
            <a href="#"><i class="material-icons">calinder</i>Request an Appointment</a>
            </div>
        </div>
    </header>

    <div class="middle">
        <?php get_template_part('modules/slider/slider.php'); ?>
    </div>

    <div class="section-one">
        
    </div>

    <div class="content">
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="content__entry">
                <?php the_content(); ?>
            </div>
        <?php endwhile; ?>
    </div>

    <div class="footer-web">For <a href="http://webdesignyorkpa.com/" title="Web Design of York">Website</a> issues, contact <a href="http://webdesignyorkpa.com/" title="Web Design of York">Web Design of York</a></div>

<?php wp_footer(); ?>
</body>
</html>
